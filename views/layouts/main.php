<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use ramosisw\CImaterial\web\MaterialAsset;

AppAsset::register($this);

?>

<?php
if (class_exists('ramosisw\CImaterial\web\MaterialAsset')) {
    ramosisw\CImaterial\web\MaterialAsset::register($this);
}
 ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets-material/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets-material/img/favicon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Material Dashboard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="assets-material/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="assets-material/css/material-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets-material/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">
    <?= $this->render('header.php') ?>
    <?= $this->render('left.php') ?>
    <div class="main-panel">
        <?= $content ?>
    </div>
    <?= $this->render('footer.php') ?>
</div>

<?php $this->endBody() ?>
</body>
<!--   Core JS Files   -->
<script src="assets-material/js/jquery-3.1.0.min.js" type="text/javascript"></script>
<script src="assets-material/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets-material/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="assets-material/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets-material/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Material Dashboard javascript methods -->
<script src="assets-material/js/material-dashboard.js"></script>

<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="assets-material/js/demo.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        // Javascript method's body can be found in assets-material/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>
</html>
<?php $this->endPage() ?>
